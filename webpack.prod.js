const {merge} = require('webpack-merge')
const common = require('./webpack.common.js')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const UglifyjsWebpackPlugin = require('uglifyjs-webpack-plugin')

module.exports = merge(common, {
  mode: 'production',
  performance: false,
  plugins: [
    new CleanWebpackPlugin(),
    new UglifyjsWebpackPlugin()
  ]
})
