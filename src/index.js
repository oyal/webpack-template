import './index.scss'

class Person {
  constructor(name, age) {
    this.name = name
    this.age = age
  }

  learn() {
    return `${this.name} is learning!`
  }
}

const oyal = new Person('oyal', 22)

console.log(oyal.learn())
console.log(oyal.name, oyal.age)
